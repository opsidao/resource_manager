use Mix.Config

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :resource_manager, ResourceManagerWeb.Endpoint,
  http: [port: 4002],
  server: false

# Print only warnings and errors during test
config :logger, level: :warn

# Configure your database
config :resource_manager, ResourceManager.Repo,
  username: System.get_env("POSTGRES_USER") || "postgres",
  password: System.get_env("POSTGRES_PASSWORD") || "postgres",
  database: System.get_env("POSTGRES_DB") || "resource_manager_test",
  hostname: System.get_env("POSTGRES_HOST") || "localhost",
  pool: Ecto.Adapters.SQL.Sandbox

# Set Mock adapter for Tesla
config :tesla, adapter: Tesla.Mock
