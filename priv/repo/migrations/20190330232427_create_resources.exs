defmodule ResourceManager.Repo.Migrations.CreateResources do
  use Ecto.Migration

  def change do
    create table(:resources) do
      add(:channel_id, :string)
      add(:enterprise_id, :string)
      add(:locked_at, :naive_datetime)
      add(:locked_by_user_id, :string)
      add(:locked_until, :naive_datetime)
      add(:name, :string)
      add(:team_id, :string)

      timestamps()
    end

    create(index(:resources, [:channel_id, :enterprise_id, :team_id, :name], unique: true))
  end
end
