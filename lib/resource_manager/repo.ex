defmodule ResourceManager.Repo do
  use Ecto.Repo,
    otp_app: :resource_manager,
    adapter: Ecto.Adapters.Postgres
end
