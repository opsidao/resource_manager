require Logger

defmodule ResourceManager.Locks.Manager do
  use Tesla

  plug Tesla.Middleware.JSON
  plug Tesla.Middleware.Retry, delay: 500, max_retries: 10

  import ResourceManager.Slack.MessageHelpers
  import ResourceManagerWeb.Gettext

  alias ResourceManager.Resources
  alias ResourceManager.Slack.Api

  def request_unlock_message(%{locked_at: locked_at} = resource) when not is_nil(locked_at) do
    resource |> unlock_message_blocks() |> message()
  end

  def lock_resource(
        %{locked_at: nil, locked_by_user_id: nil, name: name} = resource,
        user_id,
        response_url,
        locked_at \\ NaiveDateTime.utc_now()
      ) do
    Resources.update_resource(resource, %{
      locked_at: locked_at,
      locked_by_user_id: user_id
    })

    message = gettext("RESOURCE_LOCKED", resource_name: name)

    resource
    |> resource_list_blocks_with_notification(user_id, message)
    |> send_response(response_url)
  end

  def request_lock(resource, _user_id, response_url) do
    resource
    |> unlock_message_blocks()
    |> message()
    |> send_response(response_url)
  end

  def force_replace_unlock(
        %{name: name, channel_id: channel_id, locked_by_user_id: old_lock_holder_user_id} =
          resource,
        user_id,
        response_url
      ) do
    Resources.update_resource(resource, %{
      locked_at: NaiveDateTime.utc_now(),
      locked_by_user_id: user_id
    })

    message_to_old_holder =
      gettext("RESOURCE_LOCK_REPLACED_MESSAGE_OLD_HOLDER",
        resource_name: name,
        recipient_id: old_lock_holder_user_id,
        requester_id: user_id,
        channel_id: channel_id
      )

    message_to_new_holder = gettext("RESOURCE_LOCKED", resource_name: resource.name)

    Api.send_message(old_lock_holder_user_id, message_to_old_holder)

    resource
    |> resource_list_blocks_with_notification(user_id, message_to_new_holder)
    |> send_response(response_url)
  end

  def force_unlock(
        %{name: name, channel_id: channel_id, locked_by_user_id: old_lock_holder_user_id} =
          resource,
        user_id,
        response_url
      ) do
    Resources.update_resource(resource, %{locked_at: nil, locked_by_user_id: nil})

    message_to_old_holder =
      gettext("RESOURCE_LOCK_REMOVED_MESSAGE_OLD_HOLDER",
        resource_name: name,
        recipient_id: old_lock_holder_user_id,
        requester_id: user_id,
        channel_id: channel_id
      )

    message_to_new_holder = gettext("RESOURCE_UNLOCKED", resource_name: resource.name)

    if old_lock_holder_user_id != user_id do
      Api.send_message(old_lock_holder_user_id, message_to_old_holder)
    end

    resource
    |> resource_list_blocks_with_notification(user_id, message_to_new_holder)
    |> send_response(response_url)
  end

  def request_unlock_from_creator(
        %{id: id, name: name, channel_id: channel_id, locked_by_user_id: locked_by_user_id},
        requester_id,
        response_url
      ) do
    message_for_lock_creator =
      gettext("REQUEST_UNLOCK_MESSAGE",
        resource_name: name,
        recipient_id: locked_by_user_id,
        requester_id: requester_id,
        channel_id: channel_id
      )

    blocks_for_lock_creator =
      respond_to_unlock_request_blocks(
        message_for_lock_creator,
        %{
          resource_id: id,
          lock_transfer_requesting_user_id: requester_id
        }
      )

    # TODO Add action to request resource again
    message_for_unlock_requester =
      gettext("UNLOCK_REQUESTED_MESSAGE",
        resource_name: name,
        recipient_id: locked_by_user_id,
        requester_id: requester_id,
        channel_id: channel_id
      )

    Api.send_message(locked_by_user_id, message_for_lock_creator, blocks_for_lock_creator)

    send_response_in_markdown_section(message_for_unlock_requester, response_url)
  end

  def transfer_lock(
        %{name: name, channel_id: channel_id} = resource,
        lock_holder_user_id,
        new_lock_holder_user_id,
        response_url
      ) do
    Resources.update_resource(resource, %{
      locked_at: NaiveDateTime.utc_now(),
      locked_by_user_id: new_lock_holder_user_id
    })

    message_to_old_holder =
      gettext("TRANSFER_LOCK_DONE_MESSAGE_TO_OLD_HOLDER",
        resource_name: name,
        new_lock_holder_user_id: new_lock_holder_user_id,
        channel_id: channel_id
      )

    message_to_new_holder =
      gettext("TRANSFER_LOCK_DONE_MESSAGE_TO_NEW_HOLDER",
        resource_name: name,
        old_lock_holder_user_id: lock_holder_user_id,
        channel_id: channel_id
      )

    Api.send_message(new_lock_holder_user_id, message_to_new_holder)

    send_response_in_markdown_section(message_to_old_holder, response_url)
  end

  def hold_to_lock(
        %{name: name, channel_id: channel_id},
        lock_holder_user_id,
        lock_transfer_requesting_user_id,
        response_url
      ) do
    message_to_holder =
      gettext("TRANSFER_LOCK_REFUSED_MESSAGE_TO_HOLDER",
        resource_name: name,
        lock_transfer_requesting_user_id: lock_transfer_requesting_user_id,
        channel_id: channel_id
      )

    # TODO Add action to request resource again
    message_to_requester =
      gettext("TRANSFER_LOCK_REFUSED_MESSAGE_TO_REQUESTER",
        resource_name: name,
        lock_holder_user_id: lock_holder_user_id,
        channel_id: channel_id
      )

    Api.send_message(lock_transfer_requesting_user_id, message_to_requester)

    send_response_in_markdown_section(message_to_holder, response_url)
  end

  defp send_response_in_markdown_section(text, response_url) do
    text
    |> markdown_section()
    |> message()
    |> send_response(response_url)
  end

  defp send_response(payload, url) do
    Logger.info("Posting payload: #{inspect(payload)}")

    post(url, payload)
  end
end
