defmodule ResourceManager.Resources.Resource do
  use Ecto.Schema
  import Ecto.Changeset

  schema "resources" do
    field :channel_id, :string
    field :enterprise_id, :string
    field :locked_at, :naive_datetime
    field :locked_by_user_id, :string
    field :locked_until, :naive_datetime
    field :name, :string
    field :team_id, :string

    timestamps()
  end

  @doc false
  def changeset(resource, attrs) do
    resource
    |> cast(attrs, [
      :channel_id,
      :enterprise_id,
      :locked_at,
      :locked_by_user_id,
      :locked_until,
      :name,
      :team_id
    ])
    |> validate_required([:channel_id, :enterprise_id, :name, :team_id])
    |> unique_constraint(:name, name: :resources_channel_id_enterprise_id_team_id_name_index)
  end
end
