defmodule ResourceManager.Slack.Api do
  use Tesla

  plug Tesla.Middleware.Query, token: Application.get_env(:resource_manager, :slack_api)[:token]
  plug Tesla.Middleware.BaseUrl, "https://slack.com/api"
  plug Tesla.Middleware.Logger

  def send_message(user_id, text), do: send_message(user_id, text, [])

  def send_message(user_id, text, blocks) when is_list(blocks) do
    send_message(user_id, text, blocks |> Jason.encode!())
  end

  def send_message(user_id, text, blocks) when is_binary(blocks) do
    post!(
      "/chat.postMessage",
      "",
      query: [channel: user_id, text: text, blocks: blocks]
    )
  end
end
