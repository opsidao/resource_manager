defmodule ResourceManager.Slack.MessageHelpers do
  import ResourceManagerWeb.Gettext

  alias ResourceManager.Resources

  def help_blocks() do
    gettext("HELP_DETAILS") |> Jason.decode!()
  end

  def message(blocks) do
    %{
      "blocks" => List.wrap(blocks)
    }
  end

  def markdown_section(text) do
    %{
      "type" => "section",
      "text" => %{
        "type" => "mrkdwn",
        "text" => text
      }
    }
  end

  def resource_list_blocks_with_notification(resource, user_id, message) do
    notification = markdown_section(message)

    new_status =
      resource_list_blocks(%{
        "channel_id" => resource.channel_id,
        "enterprise_id" => resource.enterprise_id,
        "team_id" => resource.team_id,
        "user_id" => user_id
      })

    ([notification] ++ new_status)
    |> message()
  end

  def resource_list_blocks(%{"channel_id" => channel_id, "user_id" => user_id} = params) do
    resources = Resources.list_resources(params)

    case resources do
      [] ->
        help_blocks()

      resources ->
        header = [
          markdown_section(gettext("RESOURCE_LIST", channel_id: channel_id)),
          section_divider()
        ]

        resource_list_blocks = Enum.map(resources, &resource_list_entry(&1, user_id))

        header ++ resource_list_blocks
    end
  end

  def resource_list_entry(%{id: id} = resource, user_id) do
    %{
      "type" => "section",
      "text" => %{
        "type" => "mrkdwn",
        "text" => resource_list_entry_name(resource)
      },
      "accessory" => %{
        "type" => "button",
        "text" => %{
          "text" => resource_list_entry_accesory_text(resource),
          "type" => "plain_text",
          "emoji" => true
        },
        "value" => "#{id}",
        "action_id" => resource_list_entry_accesory_action_id(resource, user_id)
      }
    }
  end

  def section_divider() do
    %{
      "type" => "divider"
    }
  end

  defp resource_list_entry_name(%{
         name: name,
         locked_at: nil,
         locked_by_user_id: nil
       }) do
    "*#{name}*"
  end

  defp resource_list_entry_name(%{
         name: name,
         locked_at: locked_at,
         locked_by_user_id: locked_by_user_id
       }) do
    locked_at_unix = locked_at |> DateTime.from_naive!("Etc/UTC") |> DateTime.to_unix()

    gettext("LOCKED_RESOURCE_LIST_ENTRY_NAME",
      resource_name: name,
      locked_at: locked_at,
      locked_at_unix: locked_at_unix,
      locked_by_user_id: locked_by_user_id
    )
  end

  defp resource_list_entry_accesory_text(%{
         name: _name,
         locked_at: nil,
         locked_by_user_id: nil
       }) do
    gettext("LOCK_RESOURCE")
  end

  defp resource_list_entry_accesory_text(%{
         name: _name,
         locked_at: _locked_at,
         locked_by_user_id: _locked_by_user_id
       }) do
    gettext("UNLOCK_RESOURCE")
  end

  defp resource_list_entry_accesory_action_id(
         %{locked_by_user_id: locked_by_user_id},
         requesting_user_id
       ) do
    case locked_by_user_id do
      ^requesting_user_id -> "force_unlock"
      _ -> "lock_resource"
    end
  end

  def buttons_blocks(button_definitions) when is_list(button_definitions) do
    elements =
      Enum.map(button_definitions, fn {action, value, text} ->
        %{
          "text" => %{
            "emoji" => true,
            "text" => text,
            "type" => "plain_text"
          },
          "type" => "button",
          "action_id" => action,
          "value" => value
        }
      end)

    %{
      "elements" => elements,
      "type" => "actions"
    }
  end

  def unlock_message_blocks(%{id: id, locked_by_user_id: locked_by_user_id, name: name}) do
    [
      markdown_section(
        gettext("RESOURCES_IS_LOCKED_WHAT_TO_DO",
          resource_name: name,
          locked_by_user_id: locked_by_user_id
        )
      ),
      buttons_blocks([
        {"request_unlock_from_creator", "#{id}", gettext("REQUEST_POLITE_UNLOCK_ACTION")},
        {"force_replace_unlock", "#{id}", gettext("FORCE_REPLACE_UNLOCK_ACTION")},
        {"force_unlock", "#{id}", gettext("FORCE_UNLOCK_ACTION")}
      ])
    ]
  end

  def respond_to_unlock_request_blocks(message, %{
        resource_id: resource_id,
        lock_transfer_requesting_user_id: lock_transfer_requesting_user_id
      }) do
    [
      markdown_section(message),
      buttons_blocks([
        {"transfer_lock", "#{resource_id} #{lock_transfer_requesting_user_id}",
         gettext("TRANSFER_LOCK_ACTION")},
        {"hold_to_lock", "#{resource_id} #{lock_transfer_requesting_user_id}",
         gettext("HOLD_TO_LOCK_ACTION")}
      ])
    ]
  end

  def delete_resource_message(%{name: name}, deleted_by_user_id) do
    %{
      "emoji" => true,
      "response_type" => "in_channel",
      "text" =>
        gettext("RESOURCE_DELETED", resource_name: name, deleted_by_user_id: deleted_by_user_id)
    }
  end
end
