require Logger

defmodule ResourceManager.Commands.Runner do
  import ResourceManagerWeb.Gettext
  import ResourceManager.Slack.MessageHelpers

  alias ResourceManager.Resources

  def run_command(%{"text" => "create" <> name, "user_id" => user_id} = params) do
    case name |> String.trim() |> String.split(" ") do
      [""] ->
        gettext("RESOURCE_NAME_MISSING")

      [name] ->
        resource_attrs = params |> extract_identifiers() |> Map.merge(%{"name" => name})

        case Resources.create_resource(resource_attrs) do
          {:ok, _} ->
            %{
              "response_type" => "in_channel",
              "text" => gettext("RESOURCE_CREATED", name: name, creator_id: user_id)
            }

          {:error, %{errors: [{:name, {"has already been taken", _}}]}} ->
            gettext("RESOURCE_NAME_ALREADY_USED")

          unexpected ->
            Logger.error("Unexpected error creating resource: #{unexpected}")
            gettext("SORRY_I_GOT_CONFUSED")
        end

      _ ->
        gettext("RESOURCE_NAME_INVALID")
    end
  end

  def run_command(%{"text" => "delete" <> name, "user_id" => user_id} = params) do
    resource = find_resource_by_name_and_params(name, params)

    case resource do
      nil ->
        gettext("RESOURCE_NOT_FOUND", resource_name: String.trim(name))

      resource ->
        Resources.delete_resource(resource)

        delete_resource_message(resource, user_id)
    end
  end

  def run_command(%{"text" => "list" <> _, "channel_id" => _channel_id} = params),
    do: resource_list_blocks(params) |> message()

  def run_command(%{"text" => "lock"}), do: gettext("RESOURCE_NAME_MISSING")
  def run_command(%{"text" => "lock "}), do: gettext("RESOURCE_NAME_MISSING")

  def run_command(
        %{
          "text" => "lock " <> name,
          "user_id" => user_id,
          "response_url" => response_url
        } = params
      ) do
    resource = find_resource_by_name_and_params(name, params)

    case resource do
      nil ->
        gettext("RESOURCE_NOT_FOUND", resource_name: name)

      %{locked_at: nil} ->
        ResourceManager.Locks.Manager.lock_resource(resource, user_id, response_url)
        ""

      locked_resource ->
        ResourceManager.Locks.Manager.request_unlock_message(locked_resource)
    end
  end

  def run_command(%{"text" => "unlock"}), do: gettext("RESOURCE_NAME_MISSING")
  def run_command(%{"text" => "unlock "}), do: gettext("RESOURCE_NAME_MISSING")

  def run_command(
        %{
          "text" => "unlock " <> name
        } = params
      ) do
    resource = find_resource_by_name_and_params(name, params)

    case resource do
      nil ->
        gettext("RESOURCE_NOT_FOUND", resource_name: name)

      %{locked_at: nil} ->
        gettext("RESOURCE_WAS_NOT_LOCKED", resource_name: name)

      locked_resource ->
        ResourceManager.Locks.Manager.request_unlock_message(locked_resource)
    end
  end

  def run_command(%{"text" => ""} = params) do
    params = Map.merge(params, %{"text" => "list"})

    run_command(params)
  end

  def run_command(%{"text" => "help" <> _args}), do: help_blocks() |> message()
  def run_command(_args), do: help_blocks() |> message()

  defp find_resource_by_name_and_params(name, params) do
    name = String.trim(name)

    params
    |> extract_identifiers()
    |> Map.merge(%{"name" => name})
    |> Resources.find_resource()
  end

  defp extract_identifiers(%{
         "channel_id" => channel_id,
         "enterprise_id" => enterprise_id,
         "team_id" => team_id
       }) do
    %{"channel_id" => channel_id, "enterprise_id" => enterprise_id, "team_id" => team_id}
  end
end
