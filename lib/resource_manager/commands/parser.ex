defmodule ResourceManager.Commands.Parser do
  def parse(%{
        "actions" => [
          %{
            "value" => arguments,
            "action_id" => command
          }
        ],
        "channel" => %{"id" => channel_id},
        "response_url" => response_url,
        "team" => %{
          "enterprise_id" => enterprise_id,
          "id" => team_id
        },
        "user" => %{"id" => user_id, "team_id" => team_id}
      }) do
    %ResourceManager.Commands.Command{
      channel_id: channel_id,
      enterprise_id: enterprise_id,
      team_id: team_id,
      user_id: user_id,
      response_url: response_url,
      command: command,
      arguments: arguments
    }
  end

  def parse(%{
        "channel_id" => channel_id,
        "enterprise_id" => enterprise_id,
        "team_id" => team_id,
        "user_id" => user_id,
        "text" => text,
        "response_url" => response_url
      }) do
    [command | arguments] = String.split(text, " ")

    %ResourceManager.Commands.Command{
      channel_id: channel_id,
      enterprise_id: enterprise_id,
      team_id: team_id,
      user_id: user_id,
      response_url: response_url,
      command: command,
      arguments: Enum.join(arguments, " ")
    }
  end
end
