defmodule ResourceManager.Commands.Command do
  defstruct channel_id: nil,
            enterprise_id: nil,
            team_id: nil,
            user_id: nil,
            response_url: nil,
            command: nil,
            arguments: nil
end
