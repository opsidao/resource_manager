defmodule ResourceManager.Resources do
  @moduledoc """
  The Resources context.
  """

  import Ecto.Query, warn: false
  alias ResourceManager.Repo

  alias ResourceManager.Resources.Resource

  @doc """
  Returns the list of resources.

  ## Examples

      iex> list_resources()
      [%Resource{}, ...]

  """
  def list_resources do
    Repo.all(Resource)
  end

  def list_resources(%{
        "channel_id" => channel_id,
        "enterprise_id" => enterprise_id,
        "team_id" => team_id
      }) do
    query =
      from r in Resource,
        where:
          r.channel_id == ^channel_id and
            r.enterprise_id == ^enterprise_id and
            r.team_id == ^team_id,
        order_by: [asc: :name]

    Repo.all(query)
  end

  def find_resource(%{
        "channel_id" => channel_id,
        "enterprise_id" => enterprise_id,
        "team_id" => team_id,
        "name" => name
      }) do
    query =
      from r in Resource,
        where:
          r.channel_id == ^channel_id and
            r.enterprise_id == ^enterprise_id and
            r.team_id == ^team_id and
            r.name == ^name

    Repo.one(query)
  end

  @doc """
  Gets a single resource.

  Raises `Ecto.NoResultsError` if the Resource does not exist.

  ## Examples

      iex> get_resource!(123)
      %Resource{}

      iex> get_resource!(456)
      ** (Ecto.NoResultsError)

  """
  def get_resource!(id), do: Repo.get!(Resource, id)
  def get_resource(id), do: Repo.get(Resource, id)

  @doc """
  Creates a resource.

  ## Examples

      iex> create_resource(%{field: value})
      {:ok, %Resource{}}

      iex> create_resource(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_resource(attrs \\ %{}) do
    %Resource{}
    |> Resource.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a resource.

  ## Examples

      iex> update_resource(resource, %{field: new_value})
      {:ok, %Resource{}}

      iex> update_resource(resource, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_resource(%Resource{} = resource, attrs) do
    resource
    |> Resource.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a Resource.

  ## Examples

      iex> delete_resource(resource)
      {:ok, %Resource{}}

      iex> delete_resource(resource)
      {:error, %Ecto.Changeset{}}

  """
  def delete_resource(%Resource{} = resource) do
    Repo.delete(resource)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking resource changes.

  ## Examples

      iex> change_resource(resource)
      %Ecto.Changeset{source: %Resource{}}

  """
  def change_resource(%Resource{} = resource) do
    Resource.changeset(resource, %{})
  end
end
