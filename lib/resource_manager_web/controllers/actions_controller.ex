require Logger

defmodule ResourceManagerWeb.ActionsController do
  use ResourceManagerWeb, :controller

  import ResourceManager.Resources

  alias ResourceManager.Locks

  def actions(conn, %{"payload" => payload}) do
    Logger.info(payload)

    with {:ok, payload} <- Jason.decode(payload),
         {:ok, action_id, arguments} <- extract_action(payload),
         [resource_id | extra_arguments] = String.split(arguments, " "),
         resource when not is_nil(resource) <- get_resource(resource_id) do
      do_action(conn, action_id, extra_arguments, resource, payload)
    else
      {:error, reason} -> respond_with_failure(conn, reason)
      nil -> respond_with_failure(conn, "Unknown resource")
      error -> respond_with_failure(conn, "Unexpected error: #{inspect(error)}")
    end
  end

  defp extract_action(%{"actions" => [%{"action_id" => action_id, "value" => value}]}),
    do: {:ok, action_id, value}

  defp extract_action(payload),
    do: {:error, "Actions not found in payload: #{inspect(payload)}"}

  defp do_action(
         conn,
         action_id,
         extra_arguments,
         resource,
         %{
           "channel" => %{"id" => channel_id},
           "team" => %{"id" => team_id, "enterprise_id" => enterprise_id},
           "response_url" => response_url,
           "user" => %{"id" => user_id}
         }
       ) do
    resource |> inspect() |> Logger.info()

    case resource do
      %{channel_id: ^channel_id, team_id: ^team_id, enterprise_id: ^enterprise_id} ->
        execute_action(resource, action_id, extra_arguments, user_id, response_url)

        send_resp(conn, 200, "")

      %{team_id: ^team_id, enterprise_id: ^enterprise_id} ->
        if Enum.count(extra_arguments) > 0 do
          execute_action(resource, action_id, extra_arguments, user_id, response_url)

          send_resp(conn, 200, "")
        else
          Logger.warn("Missing extra_arguments")

          send_resp(conn, 400, "")
        end

      _ ->
        Logger.warn("Couldn't match existing resource with payload")

        send_resp(conn, 400, "")
    end
  end

  defp execute_action(%{locked_at: nil} = resource, "lock_resource", _, user_id, response_url),
    do: Locks.Manager.lock_resource(resource, user_id, response_url)

  defp execute_action(resource, "lock_resource", _, user_id, response_url),
    do: Locks.Manager.request_lock(resource, user_id, response_url)

  defp execute_action(resource, "force_unlock", _, user_id, response_url),
    do: Locks.Manager.force_unlock(resource, user_id, response_url)

  defp execute_action(resource, "force_replace_unlock", _, user_id, response_url),
    do: Locks.Manager.force_replace_unlock(resource, user_id, response_url)

  defp execute_action(resource, "request_unlock_from_creator", _, user_id, response_url),
    do: Locks.Manager.request_unlock_from_creator(resource, user_id, response_url)

  defp execute_action(resource, "transfer_lock", [recipient_id], user_id, response_url),
    do: Locks.Manager.transfer_lock(resource, user_id, recipient_id, response_url)

  defp execute_action(resource, "hold_to_lock", [recipient_id], user_id, response_url),
    do: Locks.Manager.hold_to_lock(resource, user_id, recipient_id, response_url)

  defp respond_with_failure(conn, message) do
    Logger.error("Failure handling actions: #{message}")
    send_resp(conn, 500, message)
  end
end
