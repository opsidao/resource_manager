defmodule ResourceManagerWeb.CommandController do
  use ResourceManagerWeb, :controller

  require Logger

  import ResourceManager.Commands.Runner, only: [run_command: 1]

  def command(conn, params) do
    Logger.info("Received params: #{inspect(params)}")

    command_result = run_command(params)

    Logger.info("Command result: #{inspect(command_result)}")

    send_command_result(conn, command_result)
  end

  defp send_command_result(conn, command_result) when is_binary(command_result),
    do: text(conn, command_result)

  defp send_command_result(conn, command_result),
    do: json(conn, command_result)
end
