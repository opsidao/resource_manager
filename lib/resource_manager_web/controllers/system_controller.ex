defmodule ResourceManagerWeb.SystemController do
  use ResourceManagerWeb, :controller

  @version ResourceManager.MixProject.project()[:version]

  def version(conn, _params) do
    conn |> json(%{version: @version})
  end
end
