defmodule ResourceManagerWeb.Router do
  use ResourceManagerWeb, :router

  use Plug.ErrorHandler
  use Sentry.Plug

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/api", ResourceManagerWeb do
    pipe_through :api

    get("/version", SystemController, :version)

    post("/command", CommandController, :command)
    post("/actions", ActionsController, :actions)
  end
end
