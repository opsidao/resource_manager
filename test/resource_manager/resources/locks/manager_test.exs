defmodule ResourceManager.Locks.ManagerTest do
  use ResourceManager.DataCase

  alias ResourceManager.Locks.Manager
  alias ResourceManager.Repo
  alias ResourceManager.Resources.Resource

  import ResourceManager.Factory
  import Tesla.Mock

  @response_url "http://a.url.com/"

  describe "request_unlock_message/1" do
    import Manager, only: [request_unlock_message: 1]

    test "when the resource is locked returns the unlock request message" do
      locked_at = NaiveDateTime.utc_now()
      locked_by_user_id = "another_user_id"

      resource =
        insert(:resource, %{
          locked_at: locked_at,
          locked_by_user_id: locked_by_user_id
        })

      expected_response = %{
        "blocks" => [
          %{
            "text" => %{
              "text" =>
                "The resource *#{resource.name}* is locked by <@#{locked_by_user_id}>, what do you want to do?",
              "type" => "mrkdwn"
            },
            "type" => "section"
          },
          %{
            "elements" => [
              %{
                "action_id" => "request_unlock_from_creator",
                "text" => %{
                  "emoji" => true,
                  "text" => "Ask the user to unlock it :see_no_evil:",
                  "type" => "plain_text"
                },
                "type" => "button",
                "value" => "#{resource.id}"
              },
              %{
                "action_id" => "force_replace_unlock",
                "text" => %{
                  "emoji" => true,
                  "text" => "Replace lock without asking :hear_no_evil:",
                  "type" => "plain_text"
                },
                "type" => "button",
                "value" => "#{resource.id}"
              },
              %{
                "action_id" => "force_unlock",
                "text" => %{
                  "emoji" => true,
                  "text" => "Just unlock it :speak_no_evil:",
                  "type" => "plain_text"
                },
                "type" => "button",
                "value" => "#{resource.id}"
              }
            ],
            "type" => "actions"
          }
        ]
      }

      assert ^expected_response = request_unlock_message(resource)

      assert %{locked_by_user_id: locked_by_user_id, locked_at: locked_at} =
               ResourceManager.Repo.get(ResourceManager.Resources.Resource, resource.id)
    end

    test "when the resource is not locked it will fail as it's an unexpected state" do
      resource = insert(:resource)

      assert_raise FunctionClauseError, fn -> request_unlock_message(resource) end
    end
  end

  describe "lock_resource/4" do
    import Manager, only: [lock_resource: 4]

    test "when the resource is unlocked, locks it and sends a message back" do
      resource = insert(:resource)
      user_id = "lock_user_id"
      expected_message_response = %Tesla.Env{status: 200}
      locked_at = ~N[2019-01-02 20:21:23]
      locked_at_unix = locked_at |> DateTime.from_naive!("Etc/UTC") |> DateTime.to_unix()

      expected_message =
        %{
          "blocks" => [
            %{
              "text" => %{"text" => "*#{resource.name}* LOCKED!", "type" => "mrkdwn"},
              "type" => "section"
            },
            %{
              "text" => %{
                "text" =>
                  "These are the resources attached to the <##{resource.channel_id}> channel",
                "type" => "mrkdwn"
              },
              "type" => "section"
            },
            %{"type" => "divider"},
            %{
              "accessory" => %{
                "action_id" => "force_unlock",
                "text" => %{
                  "emoji" => true,
                  "text" => "Unlock :unlock:",
                  "type" => "plain_text"
                },
                "type" => "button",
                "value" => "#{resource.id}"
              },
              "text" => %{
                "text" =>
                  "*#{resource.name}*\nLocked by <@#{user_id}> _<!date^#{locked_at_unix}^{date_short_pretty}, {time}|#{
                    locked_at
                  } UTC>_",
                "type" => "mrkdwn"
              },
              "type" => "section"
            }
          ]
        }
        |> Jason.encode!()

      mock(fn
        %{method: :post, url: @response_url, body: message} ->
          assert message == expected_message
          expected_message_response
      end)

      assert {:ok, ^expected_message_response} =
               lock_resource(resource, user_id, @response_url, locked_at)

      assert %{locked_at: locked_at, locked_by_user_id: ^user_id} =
               Repo.get(Resource, resource.id)

      assert locked_at
    end

    test "when the resource is locked it will fail as it's an unexpected state" do
      locked_at = NaiveDateTime.utc_now()
      locked_by_user_id = "another_user_id"

      resource =
        insert(:resource, %{
          locked_at: locked_at,
          locked_by_user_id: locked_by_user_id
        })

      assert_raise FunctionClauseError, fn ->
        lock_resource(
          resource,
          "irrelevant_user_id",
          "irrelevant_response_url",
          ~N[1970-01-01 00:00:00]
        )
      end
    end
  end

  describe "request_lock/3" do
    import Manager, only: [request_lock: 3]

    test "" do
      expected_message_response = %Tesla.Env{status: 200}
      locked_at = NaiveDateTime.utc_now()
      locked_by_user_id = "another_user_id"

      resource =
        insert(:resource, %{
          locked_at: locked_at,
          locked_by_user_id: locked_by_user_id
        })

      expected_message =
        %{
          "blocks" => [
            %{
              "text" => %{
                "text" =>
                  "The resource *#{resource.name}* is locked by <@#{locked_by_user_id}>, what do you want to do?",
                "type" => "mrkdwn"
              },
              "type" => "section"
            },
            %{
              "elements" => [
                %{
                  "action_id" => "request_unlock_from_creator",
                  "text" => %{
                    "emoji" => true,
                    "text" => "Ask the user to unlock it :see_no_evil:",
                    "type" => "plain_text"
                  },
                  "type" => "button",
                  "value" => "#{resource.id}"
                },
                %{
                  "action_id" => "force_replace_unlock",
                  "text" => %{
                    "emoji" => true,
                    "text" => "Replace lock without asking :hear_no_evil:",
                    "type" => "plain_text"
                  },
                  "type" => "button",
                  "value" => "#{resource.id}"
                },
                %{
                  "action_id" => "force_unlock",
                  "text" => %{
                    "emoji" => true,
                    "text" => "Just unlock it :speak_no_evil:",
                    "type" => "plain_text"
                  },
                  "type" => "button",
                  "value" => "#{resource.id}"
                }
              ],
              "type" => "actions"
            }
          ]
        }
        |> Jason.encode!()

      mock(fn
        %{method: :post, url: @response_url, body: ^expected_message} -> expected_message_response
      end)

      assert {:ok, ^expected_message_response} =
               request_lock(resource, "irrelevant", @response_url)
    end
  end

  describe "force_replace_unlock/3" do
    import Manager, only: [force_replace_unlock: 3]

    test "when the resource is locked, unlocks it and notifies both parties" do
      expected_message_response = %Tesla.Env{status: 200}
      new_locked_by_user_id = "new_user_id"
      old_locked_at = NaiveDateTime.add(NaiveDateTime.utc_now(), 60 * 60, :second)
      old_locked_by_user_id = "old_user_id"

      resource =
        insert(:resource, %{locked_at: old_locked_at, locked_by_user_id: old_locked_by_user_id})

      old_locked_message_expected_query = [
        channel: old_locked_by_user_id,
        text:
          ":unlock: The lock you had for *#{resource.name}* on <##{resource.channel_id}> has been replaced, now it is locked by <@#{
            new_locked_by_user_id
          }>",
        blocks: "[]",
        token: nil
      ]

      mock(fn
        %{
          method: :post,
          url: "https://slack.com/api/chat.postMessage",
          query: ^old_locked_message_expected_query
        } ->
          expected_message_response

        %{
          method: :post,
          url: @response_url
        } ->
          expected_message_response
      end)

      assert {:ok, api_response} =
               force_replace_unlock(resource, new_locked_by_user_id, @response_url)

      assert %{locked_by_user_id: new_lock_holder_user_id} = Repo.get(Resource, resource.id)
    end
  end

  describe "force_unlock/3" do
    import Manager, only: [force_unlock: 3]

    test "when the resource is locked, unlocks it and notifies both parties" do
      expected_message_response = %Tesla.Env{status: 200}
      unlocked_by_user_id = "new_user_id"
      old_locked_at = NaiveDateTime.add(NaiveDateTime.utc_now(), 60 * 60, :second)
      old_locked_by_user_id = "old_user_id"

      resource =
        insert(:resource, %{locked_at: old_locked_at, locked_by_user_id: old_locked_by_user_id})

      old_locked_message_expected_query = [
        channel: old_locked_by_user_id,
        text:
          ":unlock: The lock you had for *#{resource.name}* on <##{resource.channel_id}> has been removed by <@#{
            unlocked_by_user_id
          }>",
        blocks: "[]",
        token: nil
      ]

      mock(fn
        %{
          method: :post,
          url: "https://slack.com/api/chat.postMessage",
          query: ^old_locked_message_expected_query
        } ->
          expected_message_response

        %{
          method: :post,
          url: @response_url
        } ->
          expected_message_response
      end)

      assert {:ok, api_response} = force_unlock(resource, unlocked_by_user_id, @response_url)

      assert %{locked_by_user_id: nil, locked_at: nil} = Repo.get(Resource, resource.id)
    end
  end
end
