defmodule ResourceManager.ResourcesTest do
  use ResourceManager.DataCase

  alias ResourceManager.Resources

  describe "resources" do
    alias ResourceManager.Resources.Resource

    @valid_attrs %{
      channel_id: "some channel_id",
      enterprise_id: "some enterprise_id",
      locked_at: ~N[2010-04-17 14:00:00],
      locked_by_user_id: "some locked_by_user_id",
      locked_until: ~N[2010-04-17 14:00:00],
      name: "My resource",
      team_id: "some team_id"
    }
    @update_attrs %{
      channel_id: "some updated channel_id",
      enterprise_id: "some updated enterprise_id",
      locked_at: ~N[2011-05-18 15:01:01],
      locked_by_user_id: "some updated locked_by_user_id",
      locked_until: ~N[2011-05-18 15:01:01],
      team_id: "some updated team_id"
    }
    @invalid_attrs %{
      channel_id: nil,
      enterprise_id: nil,
      locked_at: nil,
      locked_by_user_id: nil,
      locked_until: nil,
      team_id: nil
    }

    def resource_fixture(attrs \\ %{}) do
      {:ok, resource} =
        attrs
        |> Enum.into(@valid_attrs)
        |> Resources.create_resource()

      resource
    end

    test "list_resources/0 returns all resources" do
      resource = resource_fixture()
      assert Resources.list_resources() == [resource]
    end

    test "get_resource!/1 returns the resource with given id" do
      resource = resource_fixture()
      assert Resources.get_resource!(resource.id) == resource
    end

    test "create_resource/1 with valid data creates a resource" do
      assert {:ok, %Resource{} = resource} = Resources.create_resource(@valid_attrs)
      assert resource.channel_id == "some channel_id"
      assert resource.enterprise_id == "some enterprise_id"
      assert resource.locked_at == ~N[2010-04-17 14:00:00]
      assert resource.locked_by_user_id == "some locked_by_user_id"
      assert resource.locked_until == ~N[2010-04-17 14:00:00]
      assert resource.team_id == "some team_id"
    end

    test "create_resource/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Resources.create_resource(@invalid_attrs)
    end

    test "update_resource/2 with valid data updates the resource" do
      resource = resource_fixture()
      assert {:ok, %Resource{} = resource} = Resources.update_resource(resource, @update_attrs)
      assert resource.channel_id == "some updated channel_id"
      assert resource.enterprise_id == "some updated enterprise_id"
      assert resource.locked_at == ~N[2011-05-18 15:01:01]
      assert resource.locked_by_user_id == "some updated locked_by_user_id"
      assert resource.locked_until == ~N[2011-05-18 15:01:01]
      assert resource.team_id == "some updated team_id"
    end

    test "update_resource/2 with invalid data returns error changeset" do
      resource = resource_fixture()
      assert {:error, %Ecto.Changeset{}} = Resources.update_resource(resource, @invalid_attrs)
      assert resource == Resources.get_resource!(resource.id)
    end

    test "delete_resource/1 deletes the resource" do
      resource = resource_fixture()
      assert {:ok, %Resource{}} = Resources.delete_resource(resource)
      assert_raise Ecto.NoResultsError, fn -> Resources.get_resource!(resource.id) end
    end

    test "change_resource/1 returns a resource changeset" do
      resource = resource_fixture()
      assert %Ecto.Changeset{} = Resources.change_resource(resource)
    end
  end
end
