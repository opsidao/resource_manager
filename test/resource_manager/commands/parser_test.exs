defmodule ResourceManager.Commands.ParserTest do
  use ExUnit.Case

  @channel_id "some channel id"
  @enterprise_id "entrerprise id"
  @team_id "team id"
  @user_id "user id"
  @response_url "http://response.url.com"
  @command_text "create"
  @arguments "something/preview with love"

  @action %{
    "actions" => [
      %{
        "action_ts" => "1554059985.440431",
        "block_id" => "GkC",
        "text" => %{"emoji" => true, "text" => ":lock:", "type" => "plain_text"},
        "type" => "button",
        "value" => @arguments,
        "action_id" => @command_text
      }
    ],
    "api_app_id" => "AH5HH2U2D",
    "channel" => %{"id" => @channel_id, "name" => "test_resource_manager"},
    "container" => %{
      "channel_id" => "CHG5UP4PP",
      "is_ephemeral" => true,
      "message_ts" => "1554058645.000100",
      "type" => "message"
    },
    "response_url" => @response_url,
    "team" => %{
      "domain" => "nw-red",
      "enterprise_id" => @enterprise_id,
      "enterprise_name" => "New Work",
      "id" => @team_id
    },
    "token" => "g8hzBzWtN6AJkcltZL2f9pet",
    "trigger_id" => "596137149238.560865938019.c0b7ecd03f85390c0b4db5d9ec1784a2",
    "type" => "block_actions",
    "user" => %{"id" => @user_id, "team_id" => @team_id, "username" => "juan.gonzalez"}
  }

  @command_payload %{
    "channel_id" => @channel_id,
    "enterprise_id" => @enterprise_id,
    "team_id" => @team_id,
    "user_id" => @user_id,
    "text" => "#{@command_text} #{@arguments}",
    "response_url" => @response_url
  }

  @expected_command %ResourceManager.Commands.Command{
    channel_id: @channel_id,
    enterprise_id: @enterprise_id,
    team_id: @team_id,
    user_id: @user_id,
    response_url: @response_url,
    command: @command_text,
    arguments: @arguments
  }

  describe "parse/1" do
    import ResourceManager.Commands.Parser, only: [parse: 1]

    test "generates a %Command{} for an action" do
      assert parse(@action) == @expected_command
    end

    test "generates a %Command{} for a command" do
      assert parse(@command_payload) == @expected_command
    end
  end
end
