defmodule ResourceManager.Commands.RunnerTest do
  use ResourceManagerWeb.ConnCase

  alias ResourceManager.Repo
  alias ResourceManager.Resources.Resource

  import ResourceManager.Commands.Runner
  import ResourceManager.Resources
  import ResourceManager.Slack.MessageHelpers

  import ResourceManagerWeb.Gettext

  import ResourceManager.Factory
  import Tesla.Mock

  @channel_id "some channel id"
  @enterprise_id "entrerprise id"
  @team_id "team id"
  @user_id "user id"

  def build_command(command) do
    Map.merge(
      %{
        "channel_id" => @channel_id,
        "enterprise_id" => @enterprise_id,
        "team_id" => @team_id,
        "user_id" => @user_id
      },
      command
    )
  end

  def expected_resource_locked_blocks(%{name: resource_name, id: resource_id}, locked_by_user_id) do
    %{
      "blocks" => [
        %{
          "text" => %{
            "text" =>
              "The resource *#{resource_name}* is locked by <@#{locked_by_user_id}>, what do you want to do?",
            "type" => "mrkdwn"
          },
          "type" => "section"
        },
        %{
          "elements" => [
            %{
              "text" => %{
                "emoji" => true,
                "text" => "Ask the user to unlock it :see_no_evil:",
                "type" => "plain_text"
              },
              "type" => "button",
              "value" => "#{resource_id}",
              "action_id" => "request_unlock_from_creator"
            },
            %{
              "text" => %{
                "emoji" => true,
                "text" => "Replace lock without asking :hear_no_evil:",
                "type" => "plain_text"
              },
              "type" => "button",
              "value" => "#{resource_id}",
              "action_id" => "force_replace_unlock"
            },
            %{
              "text" => %{
                "emoji" => true,
                "text" => "Just unlock it :speak_no_evil:",
                "type" => "plain_text"
              },
              "type" => "button",
              "value" => "#{resource_id}",
              "action_id" => "force_unlock"
            }
          ],
          "type" => "actions"
        }
      ]
    }
  end

  describe "create command" do
    test "creates the resource if it doesn't exist and it's valid" do
      name = "something/preview"
      command = build_command(%{"text" => "create #{name}"})
      result = run_command(command)

      expected_response = %{
        "response_type" => "in_channel",
        "text" => gettext("RESOURCE_CREATED", name: name, creator_id: @user_id)
      }

      assert ^expected_response = result
      assert list_resources() |> Enum.count() == 1

      new_resource = list_resources() |> Enum.at(0)

      assert new_resource.name == name
      assert new_resource.channel_id == @channel_id
      assert new_resource.enterprise_id == @enterprise_id
      assert new_resource.team_id == @team_id
    end

    test "creates a resource when one exists with the same name on a different channel" do
      name = "something/preview"
      command = build_command(%{"text" => "create #{name}"})

      {:ok, existing_resource} =
        %{"channel_id" => "another room", "name" => name} |> build_command() |> create_resource()

      result = run_command(command)

      expected_response = %{
        "response_type" => "in_channel",
        "text" => gettext("RESOURCE_CREATED", name: name, creator_id: @user_id)
      }

      assert ^expected_response = result
      assert [^existing_resource, new_resource] = list_resources()

      assert new_resource.name == name
      assert new_resource.channel_id == @channel_id
      assert new_resource.enterprise_id == @enterprise_id
      assert new_resource.team_id == @team_id
    end

    test "doesn't create the resource if the name is missing" do
      command = build_command(%{"text" => "create "})
      result = run_command(command)

      assert gettext("RESOURCE_NAME_MISSING") == result
      assert list_resources() |> Enum.count() == 0
    end

    test "doesn't create the resource if the name has spaces" do
      name = "something preview"
      command = build_command(%{"text" => "create #{name}"})
      result = run_command(command)

      assert gettext("RESOURCE_NAME_INVALID") == result
      assert list_resources() |> Enum.count() == 0
    end

    test "doesn't create the resource if it already exists" do
      name = "something/preview"
      command = build_command(%{"text" => "create #{name}"})

      {:ok, existing_resource} = %{"name" => name} |> build_command() |> create_resource()

      result = run_command(command)

      assert gettext("RESOURCE_NAME_ALREADY_USED") == result
      assert [^existing_resource] = list_resources()
    end
  end

  describe "delete command" do
    test "when the resource exists, it is deleted and notified to the channel" do
      {:ok, resource} =
        %{"name" => "resource1"}
        |> build_command()
        |> create_resource()

      expected_response = %{
        "emoji" => true,
        "response_type" => "in_channel",
        "text" =>
          ":wastebasket: <@#{@user_id}> has deleted the resource *#{resource.name}* from this channel"
      }

      assert ^expected_response =
               %{"text" => "delete #{resource.name}"}
               |> build_command()
               |> run_command()

      refute Repo.get(Resource, resource.id)
    end

    test "when the resource can't be found, a message is returned" do
      {:ok, resource} =
        %{"name" => "resource1", "channel_id" => "another_channel_id"}
        |> build_command()
        |> create_resource()

      expected_response = "Resource *#{resource.name}* not found"

      assert ^expected_response =
               %{"text" => "delete #{resource.name}"}
               |> build_command()
               |> run_command()

      assert Repo.get(Resource, resource.id)
    end
  end

  describe "list command" do
    test "returns the help if there are no resources" do
      expected_response = %{
        "blocks" => help_blocks()
      }

      assert ^expected_response = %{"text" => "list"} |> build_command() |> run_command()
    end

    test "lists the resources from the current channel" do
      {:ok, _} =
        %{"name" => "resource1", "channel_id" => "another_channel_id"}
        |> build_command()
        |> create_resource()

      {:ok, _} =
        %{"name" => "resource2", "channel_id" => "another_channel_id"}
        |> build_command()
        |> create_resource()

      name1 = "visible_resource_1"
      name2 = "visible_resource_2"
      name3 = "visible_resource_3"
      {:ok, resource1} = %{"name" => name1} |> build_command() |> create_resource()

      {:ok, resource2} =
        %{
          "name" => name2,
          "locked_at" => ~N[2019-02-03 10:38:00],
          "locked_by_user_id" => @user_id
        }
        |> build_command()
        |> create_resource()

      {:ok, resource3} =
        %{
          "name" => name3,
          "locked_at" => ~N[2019-02-03 10:38:00],
          "locked_by_user_id" => "another_user_id"
        }
        |> build_command()
        |> create_resource()

      expected_response = %{
        "blocks" => [
          %{
            "type" => "section",
            "text" => %{
              "type" => "mrkdwn",
              "text" => gettext("RESOURCE_LIST", channel_id: @channel_id)
            }
          },
          %{
            "type" => "divider"
          },
          %{
            "type" => "section",
            "text" => %{
              "type" => "mrkdwn",
              "text" => "*#{name1}*"
            },
            "accessory" => %{
              "text" => %{"text" => "Lock :lock:", "type" => "plain_text", "emoji" => true},
              "type" => "button",
              "value" => "#{resource1.id}",
              "action_id" => "lock_resource"
            }
          },
          %{
            "type" => "section",
            "text" => %{
              "type" => "mrkdwn",
              "text" =>
                "*#{name2}*\nLocked by <@#{resource2.locked_by_user_id}> _<!date^1549190280^{date_short_pretty}, {time}|2019-02-03 10:38:00 UTC>_"
            },
            "accessory" => %{
              "text" => %{"text" => "Unlock :unlock:", "type" => "plain_text", "emoji" => true},
              "type" => "button",
              "value" => "#{resource2.id}",
              "action_id" => "force_unlock"
            }
          },
          %{
            "type" => "section",
            "text" => %{
              "type" => "mrkdwn",
              "text" =>
                "*#{name3}*\nLocked by <@#{resource3.locked_by_user_id}> _<!date^1549190280^{date_short_pretty}, {time}|2019-02-03 10:38:00 UTC>_"
            },
            "accessory" => %{
              "text" => %{"text" => "Unlock :unlock:", "type" => "plain_text", "emoji" => true},
              "type" => "button",
              "value" => "#{resource3.id}",
              "action_id" => "lock_resource"
            }
          }
        ]
      }

      assert ^expected_response = %{"text" => "list"} |> build_command() |> run_command()
    end
  end

  describe "lock command" do
    @response_url "https://responseurl.uk.co"

    setup do
      mock(fn
        %{method: :post, url: @response_url} -> %Tesla.Env{status: 200}
      end)

      :ok
    end

    test "requests the locking if the resource is not locked" do
      name = "the_resource_name"

      {:ok, _resource} =
        %{"name" => name}
        |> build_command()
        |> create_resource()

      assert "" =
               %{"text" => "lock #{name}", "response_url" => @response_url}
               |> build_command()
               |> run_command()
    end

    test "returns a message with actions if the resource is locked" do
      name = "the_resource_name"
      locked_by_user_id = "another user id"

      {:ok, resource} =
        %{
          "name" => name,
          "locked_at" => NaiveDateTime.utc_now(),
          "locked_by_user_id" => locked_by_user_id
        }
        |> build_command()
        |> create_resource()

      expected_result = expected_resource_locked_blocks(resource, locked_by_user_id)

      assert expected_result ==
               %{"text" => "lock #{name}", "response_url" => @response_url}
               |> build_command()
               |> run_command()
    end

    test "complains if the resource name is not provided" do
      expected_result = gettext("RESOURCE_NAME_MISSING")

      assert ^expected_result =
               %{"text" => "lock", "response_url" => @response_url}
               |> build_command()
               |> run_command()

      assert ^expected_result =
               %{"text" => "lock ", "response_url" => @response_url}
               |> build_command()
               |> run_command()
    end

    test "complains if the resource can't be found" do
      name = "the_resource_name"

      expected_result = gettext("RESOURCE_NOT_FOUND", resource_name: name)

      assert ^expected_result =
               %{"text" => "lock #{name}", "response_url" => @response_url}
               |> build_command()
               |> run_command()
    end
  end

  describe "unlock command" do
    @response_url "https://responseurl.uk.co"

    setup do
      mock(fn
        %{method: :post, url: @response_url} -> %Tesla.Env{status: 200}
      end)

      :ok
    end

    test "returns a message with actions if the resource is locked" do
      name = "the_resource_name"
      locked_by_user_id = "another user id"

      {:ok, resource} =
        %{
          "name" => name,
          "locked_at" => NaiveDateTime.utc_now(),
          "locked_by_user_id" => locked_by_user_id
        }
        |> build_command()
        |> create_resource()

      expected_result = expected_resource_locked_blocks(resource, locked_by_user_id)

      assert expected_result ==
               %{"text" => "unlock #{name}", "response_url" => @response_url}
               |> build_command()
               |> run_command()
    end

    test "is confused if the resource was not locked" do
      name = "the_resource_name"

      insert(:resource, %{
        name: name,
        channel_id: @channel_id,
        enterprise_id: @enterprise_id,
        team_id: @team_id
      })

      assert ":rotating_light: *the_resource_name* was not locked... :thinking_face:" =
               %{"text" => "unlock #{name}", "response_url" => @response_url}
               |> build_command()
               |> run_command()
    end

    test "complains if the resource name is not provided" do
      expected_result = gettext("RESOURCE_NAME_MISSING")

      assert ^expected_result =
               %{"text" => "unlock", "response_url" => @response_url}
               |> build_command()
               |> run_command()

      assert ^expected_result =
               %{"text" => "unlock ", "response_url" => @response_url}
               |> build_command()
               |> run_command()
    end

    test "complains if the resource can't be found" do
      name = "the_resource_name"

      expected_result = gettext("RESOURCE_NOT_FOUND", resource_name: name)

      assert ^expected_result =
               %{"text" => "unlock #{name}", "response_url" => @response_url}
               |> build_command()
               |> run_command()
    end
  end
end
