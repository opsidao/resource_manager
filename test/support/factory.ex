defmodule ResourceManager.Factory do
  use ExMachina.Ecto, repo: ResourceManager.Repo

  def resource_factory do
    %ResourceManager.Resources.Resource{
      channel_id: sequence(:channel_id, &"channel_#{&1}"),
      enterprise_id: sequence(:enterprise_id, &"enterprise_#{&1}"),
      name: sequence(:name, &"name_#{&1}"),
      team_id: sequence(:team_id, &"team_#{&1}")
    }
  end
end
