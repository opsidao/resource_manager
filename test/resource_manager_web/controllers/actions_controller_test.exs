defmodule ResourceManagerWeb.ActionsControllerTest do
  use ResourceManagerWeb.ConnCase

  import Tesla.Mock

  @channel_id "CHG5UP4PP"
  @enterprise_id "EFSUU8PHU"
  @team_id "TGGRFTL0K"
  @name "name_of_the_resource"
  @response_url "https://madeup.hooks.slack.com/actions/TGGRFTL0K/594668582132/EEXC9JtEFXS3PZqsYp7ySZcK"

  def build_message(resource_id) do
    payload =
      %{
        "actions" => [
          %{
            "action_ts" => "1554059985.440431",
            "block_id" => "GkC",
            "text" => %{"emoji" => true, "text" => ":lock:", "type" => "plain_text"},
            "type" => "button",
            "value" => "#{resource_id}",
            "action_id" => "lock_resource"
          }
        ],
        "api_app_id" => "AH5HH2U2D",
        "channel" => %{"id" => @channel_id, "name" => "test_resource_manager"},
        "container" => %{
          "channel_id" => "CHG5UP4PP",
          "is_ephemeral" => true,
          "message_ts" => "1554058645.000100",
          "type" => "message"
        },
        "response_url" => @response_url,
        "team" => %{
          "domain" => "nw-red",
          "enterprise_id" => @enterprise_id,
          "enterprise_name" => "New Work",
          "id" => @team_id
        },
        "token" => "g8hzBzWtN6AJkcltZL2f9pet",
        "trigger_id" => "596137149238.560865938019.c0b7ecd03f85390c0b4db5d9ec1784a2",
        "type" => "block_actions",
        "user" => %{"id" => "WG5B4N5J5", "team_id" => "TGGRFTL0K", "username" => "juan.gonzalez"}
      }
      |> Jason.encode!()

    %{
      "payload" => payload
    }
  end

  setup do
    mock(fn
      %{method: :post, url: @response_url} -> %Tesla.Env{status: 200}
    end)

    {:ok, resource} =
      ResourceManager.Resources.create_resource(%{
        "channel_id" => @channel_id,
        "team_id" => @team_id,
        "enterprise_id" => @enterprise_id,
        "name" => @name
      })

    %{
      resource: resource
    }
  end

  test "lock an unlocked resource", %{conn: conn, resource: resource} do
    message = build_message(resource.id)

    conn = conn |> put_req_header("content-type", "application/vnd.api+json")
    conn = conn |> post("/api/actions", message)

    assert response(conn, 200)
  end
end
