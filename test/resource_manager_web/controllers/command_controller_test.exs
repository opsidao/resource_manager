defmodule ResourceManagerWeb.CommandControllerTest do
  use ResourceManagerWeb.ConnCase

  import ResourceManager.Slack.MessageHelpers

  test "POST /api/command with empty command", %{conn: conn} do
    conn =
      post(conn, "/api/command", %{
        "text" => "",
        "user_id" => "user_id",
        "user_name" => "user_name"
      })

    assert response(conn, 200)
  end

  test "POST /api/command with 'help'", %{conn: conn} do
    conn =
      post(conn, "/api/command", %{
        "text" => "help",
        "user_id" => "user_id",
        "user_name" => "user_name"
      })

    assert json_response(conn, 200) == help_blocks() |> message()
  end

  test "POST /api/command with 'help and extra things'", %{conn: conn} do
    conn =
      post(conn, "/api/command", %{
        "text" => "help and extra things",
        "user_id" => "user_id",
        "user_name" => "user_name"
      })

    assert json_response(conn, 200) == help_blocks() |> message()
  end

  test "POST /api/command with 'madeupcommand'", %{conn: conn} do
    conn =
      post(conn, "/api/command", %{
        "text" => "madeupcommand",
        "user_id" => "user_id",
        "user_name" => "user_name"
      })

    assert response(conn, 200)
  end
end
