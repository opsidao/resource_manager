defmodule ResourceManagerWeb.SystemControllerTest do
  use ResourceManagerWeb.ConnCase

  @version ResourceManager.MixProject.project()[:version]

  setup do
    %{
      expected_response: %{
        "version" => @version
      }
    }
  end

  test "/api/version", %{expected_response: expected_response, conn: conn} do
    conn = get(conn, "/api/version")

    assert ^expected_response = json_response(conn, 200)
  end
end
